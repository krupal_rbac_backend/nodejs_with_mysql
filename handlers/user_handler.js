var User = require("../models/user");
var bCrypt = require('bcrypt-nodejs');
var uuidV4 = require("../utils/utils");
var app = require("../app");

module.exports = {

    //Add User
    add_user: function (data, callback) {
        UserData = function () {
            var db = app.db;
            var sql = "INSERT INTO user (name, email) VALUES ('" + data.name + "', '" + data.email + "')";
            console.log(sql);
            db.query(sql, function (err, result) {
                if (err) throw err;
                console.log("1 record inserted");
                callback({
                    msg: "User added successfully",
                    statuscode: 200,
                    data: data
                });
            });
            // User.findOne({
            //     "username": data.username
            // }, function (err, user_data) {
            //     if (err) {
            //         callback({
            //             statuscode: 500,
            //             error: err,
            //             msg: "finding user_id database error"
            //         });
            //     } else if (user_data != null) {
            //         callback({
            //             statuscode: 304,
            //             msg: "User already exist"
            //         });

            //     } else {
            //         var add_user = new User();
            //         add_user.user_id = uuidV4.getUniqueId();;
            //         add_user.password = createHash(data.password);
            //         add_user.user_role = data.user_role;
            //         add_user.username = data.username;
            //         add_user.email = data.email;
            //         add_user.mobile = data.mobile;
            //         add_user.save(function (err, result) {
            //             if (err) {
            //                 callback({
            //                     statuscode: 500,
            //                     error: err,
            //                     msg: "Saving user, database error"
            //                 });
            //             } else {
            //                 callback({
            //                     statuscode: 200,
            //                     msg: "User added successfully",
            //                     data: add_user
            //                 });
            //             }
            //         });
            //     }
            // });
        }
        process.nextTick(UserData);
        // /* Generates hash using bCrypt*/
        // var createHash = function (password) {
        //     return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
        // }
    },

    //View User
    get_user: function (data, callback) {
        UserData = function () {
            var db = app.db;
            var sql = "SELECT * FROM user WHERE user_id= '"+ data.user_id +"'";
            console.log(sql);
            db.query(sql, function (err, result) {
                if (err) throw err;
                console.log("User details");
                callback({
                    msg: "User details",
                    statuscode: 200,
                    data: result
                });
            });
        }
        process.nextTick(UserData);
    },

    //List all users
    list_users: function (callback) {
        userData = function () {
            var db = app.db;
            var sql = "SELECT * FROM user";
            console.log(sql);
            db.query(sql, function (err, result) {
                if (err) throw err;
                console.log("User list");
                callback({
                    msg: "User list",
                    statuscode: 200,
                    data: result
                });
            });
        }
        process.nextTick(userData);
    },

    //Edit user
    edit_user: function (data, callback) {
        update_user_data = function () {
            var db = app.db;
            var sql = "UPDATE user SET email= '"+ data.email +"' WHERE user_id= '"+ data.user_id +"'";
            console.log(sql);
            db.query(sql, function (err, result) {
                if (err) throw err;
                console.log("User details updated successfully");
                callback({
                    msg: "User details updated successfully",
                    statuscode: 200,
                    data: data
                });
            });
        }
        process.nextTick(update_user_data);
    },

    //Delete user
    delete_user: function (data, callback) {
        delete_user_data = function () {
            var db = app.db;
            var sql = "DELETE FROM user WHERE user_id= '"+ data.user_id +"'";
            console.log(sql);
            db.query(sql, function (err, result) {
                if (err) throw err;
                console.log("User details deleted successfully");
                callback({
                    msg: "User details deleted successfully",
                    statuscode: 200,
                    data: data
                });
            });
        }
        process.nextTick(delete_user_data);
    }
}
