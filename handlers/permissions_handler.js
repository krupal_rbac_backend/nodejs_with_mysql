var app = require("../app");
module.exports = {

    //Add Permission
    add_permission: function (data, callback) {
        add_permission_data = function () {
            var db = app.db;
            var sql = "INSERT INTO permission (permission_name) VALUES ('" + data.permission_name + "')";
            console.log(sql);
            db.query(sql, function (err, result) {
                if (err) throw err;
                console.log("1 record inserted");
                callback({
                    msg: "Permission added successfully",
                    statuscode: 200,
                    data: data
                });
            });
        };
        process.nextTick(add_permission_data);
    },

    //List Permissions
    list_permissions: function (callback) {
        list_permissions_data = function () {
            var db = app.db;
            var sql = "SELECT * FROM permission";
            console.log(sql);
            db.query(sql, function (err, result) {
                if (err) throw err;
                console.log("Permissions list");
                callback({
                    msg: "Permissions list",
                    statuscode: 200,
                    data: result
                });
            });
        };
        process.nextTick(list_permissions_data);
    },

     //Get Permission
     get_permission: function (data, callback) {
        get_permission_data = function () {
            var db = app.db;
            var sql = "SELECT * FROM permission WHERE permission_id= '"+ data.permission_id +"'";
            console.log(sql);
            db.query(sql, function (err, result) {
                if (err) throw err;
                console.log("Permission details");
                callback({
                    msg: "Permission details",
                    statuscode: 200,
                    data: result
                });
            });
        };
        process.nextTick(get_permission_data);
    }
};
