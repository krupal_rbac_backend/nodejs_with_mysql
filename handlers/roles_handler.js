var app = require("../app");
module.exports = {

    //Add Role
    add_role: function (data, callback) {
        add_role_data = function () {
            var db = app.db;
            var sql = "INSERT INTO role (role_name) VALUES ('" + data.role_name + "')";
            console.log(sql);
            db.query(sql, function (err, result) {
                if (err) throw err;
                console.log("1 record inserted");
                callback({
                    msg: "Role added successfully",
                    statuscode: 200,
                    data: data
                });
            });
        };
        process.nextTick(add_role_data);
    },

    //Add Role Permission Function
    add_role_function_permission: function (data, callback) {
        add_role_function_permission_data = function () {
            var db = app.db;
            var sql = "INSERT INTO role_function_permission (role_id, function_id, permission_id) VALUES ('" + data.role_id + "','" + data.function_id + "','" + data.permission_id + "')";
            console.log(sql);
            db.query(sql, function (err, result) {
                if (err) throw err;
                console.log("1 record inserted");
                callback({
                    msg: "Role Permission Function added successfully",
                    statuscode: 200,
                    data: data
                });
            });
        };
        process.nextTick(add_role_function_permission_data);
    },

    //Get Role
    get_role: function (data, callback) {
        get_role_data = function () {
            Roles.findOne({
                "role_name": data.role_name
            }, function (err, role) {
                if (err) {
                    callback({
                        msg: "Finding role_id from database , an error",
                        statuscode: 500,
                    });
                } else if (role == null) {
                    callback({
                        msg: "role_id does not exist",
                        statuscode: 404,
                    });
                }
                callback({
                    msg: "Role details",
                    statuscode: 200,
                    data: role
                });
            });
        };
        process.nextTick(get_role_data);
    },

    //List Roles
    list_roles: function (callback) {
        list_roles_data = function () {
            Roles.find({}, function (err, roles) {
                if (err) {
                    callback({
                        msg: "Finding roles from database , an error",
                        statuscode: 500,
                    });
                } else if (roles == null) {
                    callback({
                        msg: "No roles found",
                        statuscode: 404,
                    });
                }
                callback({
                    msg: "List of roles",
                    statuscode: 200,
                    data: roles
                });
            });
        };
        process.nextTick(list_roles_data);
    },

    //Delete Role
    delete_role: function (data, callback) {
        delete_role_data = function () {
            if (data.role_id == undefined || data.role_id == '') {
                callback({
                    msgkey: "Role not found",
                    statuscode: 404,
                });
            } else {
                Roles.deleteOne({
                    "role_id": data.role_id
                }, function (err, role) {
                    if (err) {
                        callback({
                            msg: "Finding role_id from database , an error",
                            statuscode: 500,
                        });
                    } else if (role == null) {
                        callback({
                            msg: "role_id does not exist",
                            statuscode: 404,
                        });
                    }
                    callback({
                        msg: "Role deleted successfully",
                        statuscode: 200,
                    });
                });
            }
        }
        process.nextTick(delete_role_data);
    }
};
