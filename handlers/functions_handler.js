var app = require("../app");
module.exports = {

    //Add Function
    add_function: function (data, callback) {
        add_function_data = function () {
            var db = app.db;
            var sql = "INSERT INTO function (function_name) VALUES ('" + data.function_name + "')";
            console.log(sql);
            db.query(sql, function (err, result) {
                if (err) throw err;
                console.log("1 record inserted");
                callback({
                    msg: "Function added successfully",
                    statuscode: 200,
                    data: data
                });
            });
        };
        process.nextTick(add_function_data);
    },

    //List Functions
    list_functions: function (callback) {
        list_functions_data = function () {
            var db = app.db;
            var sql = "SELECT * FROM function";
            console.log(sql);
            db.query(sql, function (err, result) {
                if (err) throw err;
                console.log("Functions list");
                callback({
                    msg: "Functions list",
                    statuscode: 200,
                    data: result
                });
            });
        };
        process.nextTick(list_functions_data);
    },

     //Get Function
     get_function: function (data, callback) {
        get_function_data = function () {
            var db = app.db;
            var sql = "SELECT * FROM function WHERE function_id= '"+ data.function_id +"'";
            console.log(sql);
            db.query(sql, function (err, result) {
                if (err) throw err;
                console.log("Function details");
                callback({
                    msg: "Function details",
                    statuscode: 200,
                    data: result
                });
            });
        };
        process.nextTick(get_function_data);
    }
};
